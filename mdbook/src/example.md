# Este es un título

Ahora palabras en **negrilla**, *itálica*

* Lista
* Fin lista

A continuación lista numerada

1. Item 1
2. Item 2

## Un subtítulo

A continuación una tabla

|Algo acá|Otra cosa|Funal|
|:-------------:|:-------------:|:-----:|
|blk|blk|bkl|

### Un sub de subtitulo

A continuación una imagen

![imagen traída de estas fuentes](./img/catalejo-logo.png)

![imagen traída de internet](https://fondosmil.com/fondo/27347.jpg)

Link a una página

[Catalejo](https://catalejoplus.com)

Insertar un vídeo de youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/blMAa8wEhME" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Notas

Insertar una nota

>**Nota:** Todo lo que esté encerrado acá 
>```python
>def hola():
>    print('hola')
>```

## Referencias

[Documenteción oficial](https://rust-lang.github.io/mdBook/index.html)
